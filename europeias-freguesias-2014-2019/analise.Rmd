---
title: "R Notebook"
output: html_notebook
---

```{r}
needs(tidyverse, rio)
```

```{r}
info_2019 <- import("info_europeias_2019_freguesias.csv")

info_2019$brancos_nulos <- info_2019$brancos + info_2019$nulos
info_2019$per_nulos <- info_2019$brancos_nulos/info_2019$n_vontantes
```


```{r}
info_2014 <- import("info_europeias_2014_freguesias.csv")
```


```{r}
info_comparacao <- left_join(info_2019,info_2014, by = "dicofre" )

info_comparacao <- info_comparacao %>% rename("participacao_2019" = "per_votos.x")
info_comparacao <- info_comparacao %>% rename("participacao_2014" = "per_votos.y")

info_comparacao$taxa_crescimento <- (info_comparacao$participacao_2019 - info_comparacao$participacao_2014)/info_comparacao$participacao_2014

```



#Import dados eleitorais

```{r}
resultados_2019 <- import("resultados_europeias_2019_freguesias.csv")

resultados_2014 <- import("resultados_europeias_2014_freguesias.csv")


```


# Join resultados


```{r}
join_resultados <- inner_join(resultados_2019,resultados_2014, by=c("dicofre","acronym"))

join_resultados$crescimento <- (join_resultados$validVotesPercentage.x -join_resultados$validVotesPercentage.y)/join_resultados$validVotesPercentage.y

x <- join_resultados %>% filter(validVotesPercentage.x > 0) %>% filter(validVotesPercentage.y > 0)


```



# Só PS

```{r}
so_ps <- resultados_2019 %>% filter(acronym == "PS")
```



