|   |   |
|---|---|
|Fonte | Entidade Nacional para o Setor Energético|
| Metodologia| Os dados, [disponibilizados em .pdf](input/REPA-Geral.pdf), foram [convertidos em .csv usando o Tabula](input/tabula-REPA-Geral.csv). Depois, com recurso a R, foram convertidas as moradas em pontos de geolocalização usando ao API do Google.
| Análise | Os dados foram posteriormente colocados num mapa. Com a ajuda dos nossos leitores, alguns postos foram corrigidos para a sua localização correcta. Por convenicência, estes postos só foram corrigidos num [ficheiro geojson](geodata.geojson).
| Artigo(s)|[Descubra os postos de combustível com serviços mínimos mais perto de si](https://rr.sapo.pt/2019/07/26/pais/descubra-os-postos-de-combustivel-com-servicos-minimos-mais-perto-de-si/extralarge/159295/)|