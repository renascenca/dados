|   |   |
|---|---|
|Fonte | Associação Desenvolvimento Da Viticultura Duriense|
| Metodologia| Os dados, cedidos pela Associação Desenvolvimento Da Viticultura Duriense (ADVID), foram cedidos em excel e depois limpos e individualizados em várias tabelas. Os dados referentes a precipitação correspondem à precipitação média anual na região (em mm) e a temperatura corresponde à temperatura média anual em ºC. O ficheiro original tem dados detalhados por mês, mas não foram limpos.|
| Artigo(s)| [Douro. A seca antecipou as vindimas e pode mudar-lhe a paisagem](http://rr.sapo.pt/especial/92759/douro_a_seca_antecipou_as_vindimas_e_pode_mudar_lhe_a_paisagem)|