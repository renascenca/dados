|   |   |
|---|---|
|Fonte |Direção Geral do Ensino Superior|
| Metodologia| Os dados foram disponibilizados pela Direção Geral do Ensino Superior [no seu site em formato .xls](dados-ministério/vagas2019.xls) e foram posteriormente [transformados em csv](dados-tabela/vagas2019.csv) e depois [em json](dados-tabela/vagas2019_json.txt) para alimentar a tabela interativa.
| Artigo(s)|[Interativo. As médias e as vagas da 1ª fase de acesso ao Ensino Superior em 2019/2020](https://rr.sapo.pt/extralarge/158173/interativo-as-medias-e-as-vagas-da-1-fase-de-acesso-ao-ensino-superior-em-20192020)|